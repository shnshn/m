<?php

session_start();
ini_set('display_errors', 1);
error_reporting(E_ALL  );

if($_SERVER['HTTP_HOST'] == 'localhost') {
    define("M_USER", 'root');
    define("M_PASSWORD", "");
    define("M_DBNAME", "");
} else {
    define("M_USER", '');
    define("M_PASSWORD", "");
    define("M_DBNAME", "");
}
define("M_TIME_ZONE", "+00:00");


function novaigations() {
    $novigations = array(
        '/' => 'main/index',
    );

    if( isset( $novigations[ $_GET['PATH_INFO'] ] ) ) {
        $_REQUEST['PATH_INFO'] = $_GET['PATH_INFO'] = $novigations[ $_GET['PATH_INFO'] ];
    } elseif( isset( $novigations[ $_GET['PATH_INFO'].'/' ] ) ) {
        $_REQUEST['PATH_INFO'] = $_GET['PATH_INFO'] = $novigations[ $_GET['PATH_INFO'].'/' ];
    }
}

novaigations();

require_once("../m/mfiles.php");

define('CONTROLLERS_DIR', dirname(__FILE__).DIRECTORY_SEPARATOR);
define('PUBLIC_HTML', dirname(CONTROLLERS_DIR).DIRECTORY_SEPARATOR);
define('CLASSES_DIR', PUBLIC_HTML.'classes'.DIRECTORY_SEPARATOR);

function autoload_classes($class) {
    $file = CLASSES_DIR . $class . '.php';
    if(file_exists($file)) {
        require_once($file);
    }
}

spl_autoload_register('autoload_classes');

$router = new Mrouter();
$router->loader();
