<?php
/*------------------------------------------------------------*/
/**
  * @package M
  * @author Ohad Aloni
  */
/*------------------------------------------------------------*/
/**
  * requires Smarty software
  */
if ( ! class_exists("Smarty") ) {
	//require_once("Smarty-2.6.9/libs/Smarty.class.php");
	require_once("Smarty-3.1.21/libs/Smarty.class.php");
}
/*------------------------------------------------------------*/
require_once("msu.php");
require_once("Mdate.class.php");
/*------------------------------------------------------------*/
/**
  *  Mview - View class
  *
  * Mview is an extension of the class Smarty (smarty.php.net)
  *
  * @package M
  * @author Ohad Aloni
  */
class Mview extends Smarty {
	/*------------------------------------------------------------*/
	private static $_instance; //The single instance
	/*------------------------------------------------------------*/
	// messages and error require no construction with new()
	/*------------------------------------------------------------*/
	/**
	 * @var bool
	 */
	private $templateDirPath = array();
	/*------------------------------------------------------------*/
	/*
	Get an instance of the Mview
	@return Instance
	*/
	public static function getInstance() {
		if(!self::$_instance) { // If no instance then make one
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	/*------------------------------------------------------------*/
	// Magic method clone is empty to prevent duplication of connection
	//private function __clone() { }
	/*------------------------------------------------------------*/
	function __construct() {
        parent::__construct();
		if ( ! defined('SMARTY_TPL_DIR') )
			define('SMARTY_TPL_DIR', 'tpl');

		if ( ! defined('SMARTY_RUN_DIR') )
			define('SMARTY_RUN_DIR', 'smarty');

		$this->use_sub_dirs = false;
		$smartyTplDir = SMARTY_TPL_DIR ;
		$smartyRunDir = SMARTY_RUN_DIR ;
		$this->setTemplateDir( $smartyTplDir );
		$this->prependTemplateDir($smartyTplDir);
		if ( defined('M_DIR') )
			$this->appendTemplateDir(M_DIR."/tpl");
//		$this->compile_dir = "$smartyRunDir/compile/";
//		$this->config_dir = "$smartyRunDir/config/";
//		$this->cache_dir = "$smartyRunDir/cache/";

		$this->setCompileDir( "$smartyRunDir/compile/" );
		$this->setConfigDir( "$smartyRunDir/config/" );
		$this->setCacheDir( "$smartyRunDir/cache/" );

//		$this->registerFunction('msuShowTpl');
//		$this->registerFunction('msuVarDump');
//		$this->registerFunction('showMsg');
//		$this->registerFunction('showError');
//		$this->registerFunction('msuSetTitle');
//		$this->registerFunction('msuSetStatus');

//		$this->registerModifier('msuMoneyFmt');
//		$this->registerModifier('msuFloatFmt');
//		$this->registerModifier('msuDateFmt');
//		$this->registerModifier('msuDateTimePickerFmt');
//		$this->registerModifier('msuIntFmt');
//		$this->registerModifier('msuTimeFmt');
//		$this->registerModifier('htmlspecialchars');
//		$this->registerModifier('undash', 'Mdate');

		$this->assign(array(
			"yesNo" => array(
				0 => 'No',
				1 => 'Yes',
			),
		));
	}
	/*------------------------------------------------------------*/
	public function registerClass($method, $class) {
		if ( function_exists($method) )
			return(true);

		if ( $class ) {
			if ( ! class_exists($class) ) {
				require_once("$class.class.php");
				if ( ! class_exists($class) ) {
					self::error("Mview::registerClass: class $class not found");
					return(null);
				}
			}
			if ( method_exists($class, $method) )
				return($class);
			else {
				self::error("Mview::registerClass: method $method not found in $class");
				return(null);
			}
		}

		if ( method_exists("Mutils", $method) )
			return("Mutils");
		self::error("Mview::registerClass: method $method not found");
		return(null);
	}
	/*------------------------------*/
	/**
	 * register a smarty plugin function
	 *
	 * @param string can be any function or method in Mview or Mutils or the passed class
	 * @param string
	 */
	private function registerFunction($method, $class = null) {
		$callable = array($this, $method);
		if ( is_callable($callable) ) {
			//$this->register_function($method, $callable);
            $this->register->templateFunction($method, $callable);
			return;
		}
		// todo
		$class = $this->registerClass($method, $class);
		if ( ! $class )
			return;
		if ( $class === true ) {
			//$this->register_function($method, $method, false);
            $this->register->templateFunction($method, $method, false);
        } else {
			//$this->register_function($method, array($class, $method,));
            $this->register->templateFunction($method, array($class, $method,));
        }
	}
	/*------------------------------------------------------------*/
	/**
	 * register a smarty plugin filter (modifier)
	 *
	 * @param string can be any function or method in Mview or Mutils or the passed class
	 * @param string
	 */
	public function registerModifier($method, $class = null) {
		$class = $this->registerClass($method, $class);
		if ( ! $class )
			return;
		if ( $class === true )
			$this->register_modifier($method, $method);
		else
			$this->register_modifier($method, array($class, $method,));
	}
	/*------------------------------------------------------------*/
	/**
	 * prepend a template folder to the template search path
	 *
	 * for use with {msuShowTpl file=... arg1=...} - 
	 *
	 * when using include in templates, only the first $smarty->template_dir is recognized
	 *
	 */
	public function prependTemplateDir($dir) {
		array_unshift($this->templateDirPath, $dir);
	}
	/*------------------------------*/
	/**
	 * append a template folder to the template search path
	 *
	 * for use with {msuShowTpl file=... arg1=...} - 
	 *
	 * when using include in templates, only the first $smarty->template_dir is recognized
	 *
	 */
	public function appendTemplateDir($dir) {
		$this->templateDirPath[] = $dir;
	}
	/*------------------------------------------------------------*/
	private function _render($tpl) {
		if ( ! is_writable($this->compile_dir) ) {
			$pwd = trim(`pwd`);
			$this->error("Smarty Compile Dir $pwd/{$this->compile_dir} not writable");
			return(false);
		}
		if ( is_readable($tpl) )
			return($this->fetch($tpl));
        $cwd = getcwd();
		foreach ( $this->templateDirPath as $dir ) {
			if ( is_readable("$cwd/$dir/$tpl") )
				return($this->fetch("$cwd/$dir/$tpl"));
			if ( is_readable("$dir/$tpl") )
				return($this->fetch("$dir/$tpl"));
		}
		if ( isset($this->template_dir) ) {
			$td = $this->template_dir;
			if ( is_readable("$td/$tpl") )
				return($this->fetch($tpl));
		}
		$showPath = implode(":", $this->templateDirPath);
		$this->error("Mview: $tpl not found in $showPath");
		return(null);
	}
	/*------------------------------------------------------------*/
	/**
	 * return a rendered template
	 */
	public function render($tpl, $args = null) {
		//$args['globals'] = $GLOBALS;
		$args = array_merge($this->args, (is_array($args) ? $args:array()));
		//Mview::print_r($args);
		if ( is_array($args) ) {
			$this->assign($args);
			$this->assign(array('tplArgs' => $args));
		}
		$rendered = $this->_render($tpl);
		if ( is_array($args) ) {
			$keys = array_keys($args);
			$this->clearAssign($keys);
			$this->clearAssign('tplArgs');
		}
		return($rendered);
	}
	/*------------------------------------------------------------*/
	private static $holdOutput = false;
	private static $outputBuffer = "";
	/*------------------------------*/
	public static function holdOutput() {
		self::$holdOutput = true;
	}
	/*------------------------------*/
	public static function flushOutput() {
		// msgbuf had better been output by now by app, not usable after this;
		$Msession = new Msession;
		$msgBuf = $Msession->get('msgBuf');
		if ( $msgBuf )
			$Msession->set('msgBuf', array()); // sets cookie - must be bfore next...
		if ( self::$outputBuffer ) {
			echo self::$outputBuffer;
			self::$outputBuffer = "";
		}
		flush();
		@ob_flush(); // may have been turned off by ob_implicit_flush()
	}
	/*------------------------------*/
	public static function pushOutput($htmlText) {
		if ( self::$holdOutput )
			self::$outputBuffer .= $htmlText;
		else
			echo $htmlText;
	}
	/*------------------------------*/
	/**
	 * show a template
	 *
	 * @param string file name
	 * @param array list of named arguments
	 * @param fetch only return the rendered template and do not display if true
	 *
	 */
	public function showTpl($tpl = null, $args = null, $fetch = false) {
		if ( $tpl == null )
			$tpl = @$_REQUEST['tpl'];
		$fetched = $this->render($tpl, $args);
		if ( ! $fetch )
			self::pushOutput($fetched);
		return($fetched);
	}
	
	public $mainTpl = 'main.tpl';
	public function showMainTpl($tpl = null, $args = null, $fetch = false) {
		if ( $tpl == null )
			$tpl = @$_REQUEST['tpl'];
		$args['tpl_name'] = $tpl;
		
		//self::print_r($args);
		$fetched = $this->render($this->mainTpl, $args);
		if ( ! $fetch )
			self::pushOutput($fetched);
		return($fetched);
	}
	
	private $args = array();
	public function addArgs($args) {
		$this->args = array_merge($this->args, $args);
	}
    /*------------------------------*/
    public function addCss($css) {
        if(!is_array($css)) $css = array($css);
        if(!isset($this->args['css']))  $this->args['css'] = array();
        $this->args['css'] = array_merge($this->args['css'], $css);
    }
    /*------------------------------*/
    public function addJs($js) {
        if(!is_array($js)) $js = array($js);
        if(!isset($this->args['js']))  $this->args['js'] = array();
        $this->args['js'] = array_merge($this->args['js'], $js);
    }
	/*------------------------------*/
	public function permit() {
		 // allow automatic urls /Mview/showTpl?tpl=
		return(true);
	}
	/*------------------------------------------------------------*/
	public static function showRows($rows, $exportFileName = null) {
		global $Mview; // need an instance anyway

		if ( ! $rows || ! is_array($rows) || count($rows) == 0 ) {
			self::msg("No Rows");
			return;
		}
		$columns = array_keys($rows[0]);

		if ( ! $Mview )
			$Mview = new Mview;
		$Mview->showTpl("mShowRows.tpl", array(
				'columns' => $columns,
				'rows' => $rows,
				'exportFileName' => $exportFileName,
			));
	}
	/*------------------------------------------------------------*/
	private static $msgBuf = array();
	private static $isHold = true;
	/*------------------------------*/
	/**
	 * buffered messages
	 */
	public function messages() {
		return(self::$msgBuf);
	}
	/*------------------------------*/
	/**
	 * buffer requested messages and errors
	 * do not show anything at least until a further notice by flushMsgs()
	 */
	public function holdMsgs() {
		self::$isHold = true;
	}
	/*------------------------------*/
	/**
	 * flush messages and errors previously held due to a call to holdMsgs() and stop buffering
	 */
	function flushMsgs() {
		self::$isHold = false ;

		foreach ( self::$msgBuf as $msg )
			self::message($msg['msg'], $msg['iserror']);
		self::$msgBuf = array();
	}
	/*------------------------------*/
	private static function message($msg, $iserror) {
		$me = get_class()."::".__FUNCTION__."()";
		/*	if ( strstr($msg, "Cannot ") ) {	*/
			/*	Mutils::trace();	*/
			/*	self::flushOutput();	*/
			/*	exit;	*/
		/*	}	*/
		$isHtml = isset($_SERVER['REMOTE_ADDR']);
		if ( $isHtml ) {
			$color = $iserror ? "red" : "blue" ;
			$msg = htmlspecialchars($msg);
			$msg = nl2br($msg);
			$text = "<div style=\"color:$color; font-size:small; font-weight: bold;\">$msg</div>\n" ;
		}
		else {
			$pfx = $iserror ? "ERROR: " : "" ;
			$text =  "$pfx$msg\n" ;
		}
		$Msession = new Msession;
		$sessionMsgBuf = $Msession->get('msgBuf');
		if ( ! $sessionMsgBuf )
			$sessionMsgBuf = array();
		$numMessages = count($sessionMsgBuf);
		if ( $numMessages >= 7 ) {
			$lastText = $sessionMsgBuf[$numMessages-1];
			if ( $lastText != '...' ) {
				$sessionMsgBuf[] = '...';
				$Msession->set('msgBuf', $sessionMsgBuf);
			}
		} else {
			$sessionMsgBuf[] = $text;
			$Msession->set('msgBuf', $sessionMsgBuf);
		}
		self::pushOutput($text);
	}
	/*------------------------------*/
	/**
	 * show a msg (or hold it - see holdMsgs())
	 *
	 * @param string
	 */
	public static function msg($msg, $iserror = false) {
		if ( self::$isHold )
			self::$msgBuf[] = array('msg' => $msg, 'iserror' => $iserror);
		else
			self::message($msg, $iserror);
	}
	/*------------------------------*/
	/**
	 * show an error (or hold it - see holdMsgs())
	 *
	 * @param string
	 */
	public static function error($msg) {
		self::msg($msg, true);
	}
	/*------------------------------------------------------------*/
	/**
	 * show a message from a template
	 *
	 * {showMsg msg="..."}
	 */
	public function showMsg($a) { self::msg($a['msg']); }
	/*------------------------------*/
	/**
	 * show an error from a template
	 *
	 * {showError msg="..."}
	 */
	public function showError($a) { self::error($a['msg']); }
	/*------------------------------------------------------------*/
	/**
	 * a fancier version of print_r helps debugging by showing a title, file and line number
	 * in a more legible display
	 *
	 *
	 * e.g<br />
	 * Mview::print_r($_REQUEST, "_REQUEST", __FILE__, __LINE__);
	 *
	 * @param mixed
	 * @param string
	 * @param string
	 * @param int
	 * 
	 */
	public static function print_r($var, $varName = null, $file = null, $line = null, $return = false) {
		$isHtml = isset($_SERVER['REMOTE_ADDR']);
		$ret = "";
		if ( $isHtml )
			$ret .= "\n<table border=\"0\"><tr><td align=\"left\"><pre>\n";
		if ( $file ) {
			$fileParts = explode("/", trim($file, "/"));
			$fileName = $fileParts[count($fileParts)-1];
			$ret .= "$varName ($fileName: $line)\n--------------------------------------------------------------\n";
		}
		$ret .= print_r($var, true);
		$ret .= "\n";
		if ( $isHtml )
			$ret .= "\n</pre></td></tr></table>\n";
		if ( ! $return )
			self::pushOutput($ret);
		return($ret);
	}
	/**
	 * Send debug code to the Javascript console
	 */ 
	function console_log($data, $title='PHP') {
		echo("<script>console.log(".json_encode( array($title => $data) ).");</script>\n");
	}
	/*------------------------------------------------------------*/
	/**
	 *
	 * escape quotes for javascript
	 *
	 * @param string
	 * @return string
	 */
	public function jsStr($str) {
		// escape with \ but
		// if they are already escaped ...
		$ret = str_replace("\\'", "'", $str);
		$ret = str_replace("'", "\\'", $ret);
		$ret = str_replace("\r\n", "\n", $ret);
		$ret = str_replace("\\n", "\n", $ret);
		$ret = str_replace("\n", "\\n", $ret);
		return($ret);
	}
	/*------------------------------*/
	/**
	 * execute javascript by echoing it wrapped in html and flushing output buffers for immeadiate execution
	 *
	 * @param string
	 */
	public function js($s) {
		echo "<script type=\"text/javascript\"> $s </script>\n" ;
		flush();
		ob_flush();
	}
	/*------------------------------*/
	/**
	 * set title of page using javascript
	 *
	 * @param string
	 */
	public function jsTitle($s) {
		$title = $this->jsStr($s);
		$this->js("document.title = '$title' ; ");
		
	}
	/*------------------------------------------------------------*/
	public static function setCookie($name, $value, $expires = null) {
		if ( $expires < 0 ) {
			unset($_COOKIE[$name]);
			@setcookie($name, null, time(0) + 3, "/");
			return;
		}
		$defaultExpires = 10*365*24*60*60;
		if ( $expires  == null )
			$expires = $defaultExpires;
		if ( $expires  <= $defaultExpires )
			$expires += time();
		if ( @setcookie($name, $value, $expires, "/") ) {
			$_COOKIE[$name] = $value;
		} else {
			/*	self::error("Cannot set cookie - output already started");	*/
			/*	Mutils::trace();	*/
			/*	self::flush();	*/
			/*	exit;	*/
		}
	}
	/*------------------------------------------------------------*/
	/**
	 * include a template
	 * {msuShowTpl file="abc.tpl" a=... b=... c=...}
	 */
	public function msuShowTpl($a) {
		$tpl = $a['file'];
		$b = $a ;
		$b['tplArgs'] = $a;
		$rendered = $this->showTpl($tpl, $b, true);
		// call from a smarty template -
		// skip output buffering or output will be reversed.
		echo $rendered;
	}
	/*------------------------------------------------------------*/
}

/*------------------------------------------------------------*/
