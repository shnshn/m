<?php
/*------------------------------------------------------------*/
/**
  * @package M
  * @author Ohad Aloni
  */
/*------------------------------------------------------------*/

/*------------------------------------------------------------*/
/**
  * 
  * Mcontroller has several control functions:
  * 1. Allow control to flow only if appropriate permission conditions are met.<br />
  * 2. Branch URLs of the form className=...&action=... to the corresponding class method.<br />
  * 3. Serve as a superclass to be extended with seamless inheritance from Mmoel and Mview.<br />
  *
  * @package M
  */
/*------------------------------------------------------------*/
class Mcontroller {
    protected $registry;
	private $isConstructed = false;
	/**
	* @var Mmodel access the Mmodel class from this instance
	*/
	public $Mmodel;
	/**
	* @var Mview access the Mview class from this instance
	*/
	public $Mview;
	/*------------------------------------------------------------*/
    /**
	 * Mcontroller is typically extended with no arguments
	 * or created with no arguments.
	 *
	 * @param Mmodel force use of this instance
	 * @param Mview force use of this instance
	 *
	 */
	function __construct($Mm = null, $Mv = null) {

        $this->registry = Mregistry::getInstance();
        $this->Mmodel = Mmodel::getInstance();
		$this->Mview = Mview::getInstance();

		if ( ! $this->Mmodel ) {
			$stack = debug_backtrace(false);
			Mview::print_r($stack, "stack", __FILE__, __LINE__);
			exit;
		}
		if ( $this->Mmodel->isConnected() )
			$this->isConstructed = true;

        $this->registry->logedIn = ( isset($_SESSION['logedIn']) && $_SESSION['logedIn'] == true ) ? true : false;

        if( $this->registry->logedIn ) {
            $this->registry->main_template = 'main.registered.tpl';
        } else {
            $this->registry->main_template = 'main.tpl';
        }

        $this->Mview->mainTpl = $this->registry->main_template;

	}


    /*------------------------------*/
    public function isConstructed() {
		return($this->isConstructed);
	}
	/*------------------------------------------------------------*/

	/*------------------------------*/
    public function before() {}
    public function after() {}
	/*------------------------------------------------------------*/
	public function redirect($url = null)  {
		if ( $url && substr($url, 0, 4) == "http" ) {
			header("Location: $url");
			exit;
		}
		if ( ! $url ) {
			$url = $this->controller;
			if ( $this->action )
				$url .= "/".$this->action;
		}
		if ( $url == "/" )
			$url = "";
		$serverName = $_SERVER['SERVER_NAME'];
		$isHttps = @$_SERVER['HTTPS'] == "on";
		$s = $isHttps ? "s" : "";
		$url = trim($url, "/");
		header("Location: http$s://$serverName/$url");
		exit;
	}
	/*------------------------------------------------------------*/
	/**
	 * decide whether to allow the execution of a method by the user
	 *
	 * permit() is by default suitable for public access.
	 * It returns true thus allowing everything.<br />
	 * In secure and controlled systems, permit() is overridden by the extending class
	 * to check login credentials
	 * 
	 * @return bool
	 */
	 public function permit() {
	 	return(true);
	 }
	/*------------------------------------------------------------*/
	/**
	 * when a URL specifies a controller without an action
	 * the method index() is called.
	 *
	 */

	public function index() {
		$className = get_class($this);
		$this->Mview->error("$className: method index() not defined"); //404
		//print_r($_GET);
		return(null);
	}
	/*------------------------------*/
	public function defaultAction() { $this->index(); }
	public function main() { $this->index(); }
	/*------------------------------------------------------------*/

    public function not_found() {
        $this->Mview->showMainTpl('404.tpl');
    }

    public function permission_denied() {
        $this->Mview->showMainTpl('permission_denied.tpl');
    }
}
/*------------------------------------------------------------*/
