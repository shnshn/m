<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>{$title}</title>
	<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.hoverClass.js"></script>

	<link type="text/css" href="js/jquery-ui-1.8.custom/development-bundle/themes/base/jquery.ui.all.css" rel="stylesheet" />
	<link type="text/css" href="js/jquery-ui-1.8.custom/development-bundle/themes/base/jquery.ui.dialog.css" rel="stylesheet" />
	<script type="text/javascript" src="js/jquery-ui-1.8.custom/development-bundle/ui/jquery-ui-1.8.custom.js"></script>

	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript" src="js/jquery.form.js"></script>

	<link rel="stylesheet" type="text/css" href="css/M.css"></link>
	<script type="text/javascript" src="js/jquery.jeditable.js"></script>
	<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
	<script type="text/javascript" src="js/jquery.jeditable.wysiwyg.js"></script>
	<script type="text/javascript" src="js/jquery-autocomplete/jquery.autocomplete.js"></script>
	<link rel="stylesheet" href="js/jquery-autocomplete/jquery.autocomplete.css" type="text/css"></link>
	<script type="text/javascript" src="js/jquery.Mautocomplete.js"></script>
	<script type="text/javascript" src="js/jquery-tooltip/lib/jquery.dimensions.js"></script>
	<script type="text/javascript" src="js/jquery-tooltip/jquery.tooltip.js"></script>
	<script type="text/javascript" src="js/jquery.imgToolTip.js"></script>
	<script type="text/javascript" src="js/jquery.showImage.js"></script>
	<link rel="stylesheet" href="css/jquery.wysiwyg.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="js/jquery-tooltip/jquery.tooltip.css"></link>
	<script type="text/javascript" src="js/Mutils.js"></script>
	<script type="text/javascript" src="js/M.js"></script>
	<script type="text/javascript" src="js/sound.js"></script>
</head>
<body>
