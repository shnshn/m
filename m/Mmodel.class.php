<?php
/*------------------------------------------------------------*/
/**
  * Mmodel - mysql convenience utilities
  *
  * @package M
  * @author Ohad Aloni
  */
/*------------------------------------------------------------*/
/**
 * Mmodel may be used independently,
 * It might use Mview to display some error messages
 */
require_once("Mview.class.php");
/*------------------------------------------------------------*/
/**
  * Mmodel - mysql convenience utilities
  *
  * @package M
  * @author Ohad Aloni
  */
class Mmodel {
	/*------------------------------------------------------------*/
	private static $_instance; //The single instance
	/*------------------------------------------------------------*/
	private $isConnected = false;
	/*------------------------------------------------------------*/
	private $dbHost = null;
	private $dbUser = null;
	private $dbPasswd = null;
	private $dbName = null;
	/*------------------------------*/
	private $dbHandle = null;
	private $lastSql = null;
	private $lastError = null;
	private $lastInsertId = null;
	/*------------------------------*/
	private $Mmemcache = null;
	/*------------------------------------------------------------*/
	/*
	Get an instance of the Database
	@return Instance
	*/
	public static function getInstance() {
		if(!self::$_instance) { // If no instance then make one
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	/*------------------------------------------------------------*/
	// Magic method clone is empty to prevent duplication of connection
	private function __clone() { }
	/*------------------------------------------------------------*/
	public function __construct($user = null, $passwd = null, $dbName = null,  $host = null) {
		if ( $host )
			$this->dbHost = host;
		elseif ( defined('M_HOST') )
			$this->dbHost = M_HOST;
		else
			$this->dbHost = 'localhost';

		if ( $dbName )
			$this->dbName = $dbName;
		elseif ( defined('M_DBNAME') && M_DBNAME != 'none' )
			$this->dbName = M_DBNAME;
		else
			$this->dbName = null;

		if ( $user )
			$this->dbUser = $user;
		elseif ( defined('M_USER') )
			$this->dbUser = M_USER;
		else
			$this->dbUser = null;

		if ( $passwd )
			$this->dbPasswd = $passwd;
		elseif ( defined('M_PASSWORD') )
			$this->dbPasswd = M_PASSWORD;
		else
			$this->dbPasswd = null;

		if ( ! $this->dbUser || $this->dbPasswd === null ) {
			Mview::error("Must define M_USER M_PASSWORD or construct Mmodel with arguments");
			return;
		}
		if ( ! $this->selectHost($this->dbHost, $this->dbUser, $this->dbPasswd, $this->dbName) )
			return;
		$this->isConnected = true;

        if( defined("M_TIME_ZONE") ) {
            mysql_query("SET time_zone='".M_TIME_ZONE."';");
        }

	}
	/*------------------------------*/
	public function isConnected() {
		return($this->isConnected);
	}
	/*------------------------------------------------------------*/
	public function selectHost($dbHost, $dbUser, $dbPasswd, $dbName) {
		$this->dbHost = $dbHost;
		$this->dbUser = $dbUser;
		$this->dbPasswd = $dbName;
		$this->dbName = $dbName;
		$this->dbHandle = @mysql_connect($dbHost, $dbUser, $dbPasswd);
		if ( ! $this->dbHandle ) {
			$error = mysql_error();
			$this->lastError = $error;
			Mview::error("Could not connect to db: $error");
			return(false);
		}
		$res = $this->query("SET NAMES 'utf8'");
		if ( ! $res ) {
		}
		if ( $dbName )  {
			if ( ! $this->selectDb($dbName) ) {
				$error = @mysql_error() ;
				$this->lastError = $error;
				Mview::error("Unable to select db $dbName: $error");
			}
		}
		return(true);
	}
	/*------------------------------------------------------------*/
	/**
	  * use database
	  */
	public function selectDB($db) {
		$this->dbName = $db;
		$ret = @mysql_select_db($this->dbName);
		if ( $ret == false ) {
			$error = @mysql_error() ;
			$this->lastError = $error;
			return(false);
		}
		return(true);
	}
	/*------------------------------*/
	/**
	  * use database
	  */
	public function useDB($db) {
		return($this->selectDB($db));
	}
	/*------------------------------------------------------------*/
	/**
	 * id of last insert - same as mysql_insert_id()
	 *
	 * @return int
	 */
	public function insertId() {
		return($this->lastInsertId);
	}
	/*------------------------------------------------------------*/
	/**
	 * make a string usable in quotes of sql statement
	 * @param string
	 * @return string
	 */
	public function str($str) {
		if ( ! $str )
			return($str);
		// if they are already escaped
		$ret = mysql_real_escape_string($str);
		//$ret = str_replace("\\'", "'", $str);
		//$ret = str_replace("'", "\\'", $ret);
		//$ret = str_replace("\r\n", "\n", $ret);
		//$ret = str_replace("\n", "\\n", $ret);
		return($ret);
	}
	/*------------------------------------------------------------*/
	/**
	 * make a string usable in quotes of sql statement
	 * @param string
	 * @param string to escape
	 * @return string
	 */
	public function dbString() {
		$args = func_get_args();
		if (count($args) == 0)
			return '';
		
		$query = array_shift($args);
		
		if (count($args) > 0) {
			$args = array_map('mysql_real_escape_string', $args);
			$query = vsprintf( $query, $args );   
		}
		
		return $query;		
	}
	/*------------------------------------------------------------*/
	/**
	 * query() is almost synonim to mysql_query().<br />
	 * It is used for streaming when possibly large data sets are expected.
	 *
	 * For queries expecting a single row, item or a relativelly small result set,<br />
	 * use getRow(), getRows(), getString(), getStrings() or getInt()<br />
	 * for a complete "round trip" and single-line-coding convenience
	 *
	 * @param string the sql query
	 * @return resource as returned from mysql_query()
	 */
	public function query($sql) {
		$res = null;
		try {
			$res = @mysql_query($sql);
		} catch (Exception $e) {
			$msg = $e->getMessage();
			Mview::error($msg);
			$error = @mysql_error();
			$this->lastError = $error;
			if ( $error )
				Mview::error("sql error: $error");
			Mview::error($sql);
			return(null);
		}
		if ( ! $res ) {
			$error = @mysql_error();
			$this->lastError = $error;
			if ( $error )
				Mview::error("sql error: $error");
			Mview::error($sql);
			if ( stristr($error, "MySQL server has gone away") ) {
				Mview::error("EXITTING");
				exit; // servers will auto restart
			}
			return(null);
		}
		return($res);
	}
	/*------------------------------------------------------------*/
	/**
	 * execute a query, expecting no particular resulting data.
	 *
	 * @param string the query
	 * @param bool used internally (see dbLog())
	 * @return int number of rows affected by the query
	 */

	public function _sql($sql, $rememberLastSql = true) {
		$res = $this->query($sql);
		if ( ! $res ) {
			return(null);
		}
		if ( $rememberLastSql )
			$this->lastSql = $sql;
		$affected = @mysql_affected_rows();
		@mysql_free_result($res);
		return($affected);
	}
	/*------------------------------*/
	/**
	 * execute a query, expecting no particular resulting data.
	 *
	 * @param string the query
	 * @return int number of rows affected by the query
	 */
	public function sql($sql) {
		if ( ! $this->isConnected ) {
			Mview::error("Mmodel not connected");
			return(null);
		}
		$ret = $this->_sql($sql);
		if ( strstr($sql, 'insert') )
			$this->lastInsertId = @mysql_insert_id();
		if ( $ret > 0 )
			$this->dbLog('', 'sql', 0);

		return($ret);
	}
	/*----------------------------------------*/
	/**
	 * get rows from the database
	 *
	 * @param string the query
	 * @return array array of associative arrays from mysql_fetch_assoc()
	 */
	public function getRows($sql, $ttl = null) {
		if ( $ttl !== null && ! $this->Mmemcache )
			$this->Mmemcache = new Mmemcache;
		$memcacheKey = get_class().":8:".__FUNCTION__."('$sql')";
		if ( $ttl !== null && ($rows = $this->Mmemcache->get($memcacheKey)) !== false ) {
			return($rows);
		}
		if ( ! $this->isConnected ) {
			Mview::error("Mmodel not connected");
			return(null);
		}
		$res = $this->query($sql);
		if ( ! $res ) {
			return(null);
		}
		$ret = array();
		while($r = @mysql_fetch_assoc($res))
			$ret[] = $r ;
		@mysql_free_result($res);
		if ( $ttl !== null )
			$this->Mmemcache->set($memcacheKey, $ret, $ttl);
		return($ret);
	}
	/*----------------------------------------*/
	/**
	 * get a single row from the database
	 *
	 * @param string the query
	 * @return array associative array from mysql_fetch_assoc()
	 */
	public function getRow($sql, $ttl = null) {
		if ( ! $this->isConnected ) {
			Mview::error("Mmodel not connected");
			return(null);
		}
		$rows = $this->getRows($sql, $ttl);
		if ( count($rows) == 0 )
			return(null);
		return($rows[0]);
	}
	/*------------------------------*/
	/**
	 * get a row from a table by its id
	 *
	 * @param string the table from which the data is fetched
	 * @param int the id value
	 * @param string the name of the id field if it is not 'id'
	 * @return array associative array of data of the row
	 */
	public function getById($tableName, $id, $idName = "id", $ttl = null) {
		return($this->getRow("select * from $tableName where $idName = $id", $ttl));
	}
	/*------------------------------*/
	/**
	 * get a rows from a table by its id
	 *
	 * @param string the table from which the data is fetched
	 * @param int the id value
	 * @param string the name of the id field if it is not 'id'
	 * @return array associative array of data of the row
	 */
	public function getRowsById($tableName, $id, $idName = "id", $ttl = null) {
		return($this->getRows("select * from $tableName where $idName = $id", $ttl));
	}
	/*----------------------------------------*/
	/**
	 * get a column of data from the database
	 *
	 * @param string the query
	 * @return array an array with the data
	 */
	public function getStrings($sql, $ttl = null) {
		$rows = $this->getRows($sql, $ttl);
		if ( $rows === null )
			return(null);
		$ret = array();
		foreach ( $rows as $row )
			$ret[] = array_shift($row); // take the value of the first (and only) column, ignoring the index field name
		return($ret);
	}
	/*----------------------------------------*/
	/**
	 * get an item (single row, single column) from the database
	 *
	 * @param string the query
	 * @return string the item data
	 */
	public function getString($sql, $ttl = null) {
		if ( ! $this->isConnected ) {
			Mview::error("Mmodel not connected");
			return(null);
		}
		$strings = $this->getStrings($sql, $ttl);
		if ( $strings )
			return($strings[0]);
		else
			return(null);
	}
	/*----------------------------------------*/
	/**
	 * get an int item from the database
	 *
	 * @param string the query
	 * @return int the returned number
	 */
	public function getInt($sql, $ttl = null) {
		if ( ! $this->isConnected ) {
			Mview::error("Mmodel not connected");
			return(null);
		}
		if ( ($ret = $this->getString($sql, $ttl)) === null )
			return(null);

		return((int)$ret);
	}
	/*------------------------------------------------------------*/
	/**
	 * name of the auto_increment column in table
	 *
	 * @param string the name of the table
	 * @return string name of auto_increment column
	 */
	public function autoIncrement($tableName) {
		if ( ! $this->Mmemcache )
			$this->Mmemcache = new Mmemcache;
		static $cache = array();

		if ( isset($cache[$tableName]) )
			return($cache[$tableName]);

		$memcacheKey = get_class().":1:".__FUNCTION__."('$tableName')";
		if ( ($cache[$tableName] = $this->Mmemcache->get($memcacheKey)) != null )
			return($cache[$tableName]);

		$fields = $this->fields($tableName);
		if ( ! $fields ) {
			$cache[$tableName] = null;
			return($cache[$tableName]);
		}
		foreach ( $fields as $field ) {
			if ( $field['isAutoInc'] ) {
				$cache[$tableName] = $field['name'];
				break;
			}
		}
		if ( ! isset($cache[$tableName]) )
			$cache[$tableName] = null;
		if ( $cache[$tableName] )
			$this->Mmemcache->set($memcacheKey, $cache[$tableName], 24*60*60);
		return($cache[$tableName]);
	}
	/*------------------------------------------------------------*/
	public function typeGroup($ftype) {
		if ( strncmp($ftype, 'int', 3) == 0 )
			return("int");
		if ( $ftype == 'text' )
			return("text");
		if ( strncmp($ftype, 'varchar', 7) == 0 )
			return("text");
		if ( in_array($ftype, array('date', 'datetime', 'timestamp')) )
			return("date");
		return($ftype);
	}
	/*------------------------------------------------------------*/
	/**
	 * schema information for a table:
	 *
	 * @param string the name of the table
	 * @return array two dimensional array. For each column in the table: name, type, isNull, isPrimary, default, isAutoInc
	 */
	public function fields($tableName) {
		static $cache = array();

		if ( isset($cache[$tableName]) )
			return($cache[$tableName]);

		$columnRows = $this->getRows("show columns from $tableName", 2*3600);
		if ( ! $columnRows ) {
			Mview::error("No columns for $tableName");
			$cache[$tableName] = null ;
			return($cache[$tableName]);
		}
		$fields = array();
		foreach ( $columnRows as $col )
			$fields[] = array(
				'name' => $col['Field'],
				'type' => $col['Type'],
				'typeGroup' => $this->typeGroup($col['Type']),
				'isNull' => $col['Null'] == 'YES' ? true : false,
				'isPrimary' => $col['Key'] == 'PRI',
				'default' => $col['Default'],
				'isAutoInc' => $col['Extra'] == 'auto_increment',
				'isKey' => $col['Key'] != null,
			);
		$cache[$tableName] = $fields ;
		return($cache[$tableName]);
	}
	/*----------------------------------------*/
	/**
	 * schema of one field
	 */
	 public function field($tableName, $fieldName) {
	 	$fields = $this->fields($tableName);
		if ( ! $fields )
			return(null);
		foreach ( $fields as $field )
			if ( $field['name'] == $fieldName )
				return($field);
		return(null);
	 }
	/*----------------------------------------*/
	/**
	 * list fields of a table
	 *
	 * @param string the name of the table
	 * @return array list of field names
	 */
	public function columns($tableName) {
		static $cache = array();

		if ( isset($cache[$tableName]) )
			return($cache[$tableName]);

		$columnRows = $this->getRows("show columns from $tableName", 2*3600);
		if ( ! $columnRows ) {
			Mview::error("No columns for $tableName");
			$cache[$tableName] = null ;
			return($cache[$tableName]);
		}
		$cols = array();
		foreach ( $columnRows as $col )
			$cols[] = $col['Field'] ;
		$cache[$tableName] = $cols ;
		return($cache[$tableName]);
	}
	/*----------------------------------------*/
	/**
	  * does field exist in a table
	  *
	  * @param string the name of the table
	  * @param string the name of the field
	  * @return bool
	  */
	public function isColumn($tableName, $column) {
		$columns = $this->columns($tableName);
		if ( ! $columns )
			return(false);
		return(in_array($column, $columns));
	}
	/*----------------------------------------*/
	/**
	  * number of rows in a table
	  *
	  * @param string the name of the table
	  * @return int the number of rows in the table
	  */
	public function rowNum($t) {
		return($this->getInt("select count(*) from $t"));
	}
	/*----------------------------------------*/
	/**
	  * list of tables in database
	  *
	  * @param string the database name (optional - if not the default database)
	  * @return array list of tables
	  */
	public function tables($db = null) {
		static $cache = null;
		if ( ! $db )
			$db = $this->dbName;

		if ( ! $db )
			return(false);

		if ( isset($cache[$db]) )
			return($cache[$db]);
		$cache[$db] = $this->getStrings("show tables from $db", 5*60);
		return($cache[$db]);
	}
	/*----------------------------------------*/
	/**
	 * list of databases
	 */
	public function databases() {
		static $cache = null;
		static $excludes = array('information_schema', 'mysql', 'test',);
		$dbHost = $this->dbHost;
		if ( isset($cache[$dbHost]) )
			return($cache[$dbHost]);
		$allDatabases = $this->getStrings("show databases");
		$databases = array();
		foreach ( $allDatabases as $db )
			if ( ! in_array($db, $excludes) )
				$databases[] = $db;
		$cache[$dbHost] = $databases;
		return($cache[$dbHost]);
	}
	/*----------------------------------------*/
	/**
	 * does table exist
	 *
	 * @param string the table name
	 * @return bool
	 */
	public function isTable($t) {
		$pair = explode('.', $t);
		if ( count($pair) == 2 ) {
			$dbname = $pair[0];
			$tname = $pair[1];
			$sql = "select count(*) from information_schema.tables where table_schema = '$dbname' and table_name = '$tname'";
			return($this->getInt($sql));
		}
		$tables = $this->tables();
		// with xampp, all table names are lowercase but $t might not be
		return(in_array($t, $tables) || in_array(strtolower($t), $tables));
	}
	/*------------------------------------------------------------*/
	/**
	  * dbInsert() without logging (see dbLog())
	  *
	  * @param string table to insert the data to
	  * @param array associative array with data. Fields not matching columns of the table are silently ignored. 
	  * @return int auto-increment id of the new row
	  */
	 
	public function _dbInsert($tableName, $data, $rememberLastSql = true, $withId = false) {
		if ( ! $this->isConnected ) {
			Mview::error("Mmodel not connected");
			return(null);
		}

		if ( ($sql = $this->dbInsertSql($tableName, $data, $withId)) == null )
			return(null);

		$affected = $this->_sql($sql, $rememberLastSql);
		if (  $affected != 1 )
			return(null);
		$this->lastInsertId = mysql_insert_id();
		return($this->lastInsertId);
	}
	/*------------------------------*/
	/**
	  * insert data to database
	  *
	  * @param string table to insert the data to
	  * @param array associative array with data. Fields not matching columns of the table are silently ignored. 
	  * @return int auto-increment id of the new row
	  */
	public function dbInsert($tableName, $data, $withId = false) {
		if ( ($id = $this->_dbInsert($tableName, $data, true, $withId)) == null )
			return(null);
		$this->dbLog($tableName, 'insert', $id);
		return($id);
	}
	/*------------------------------------------------------------*/
	/**
	  * update a row of data - raw interface - see also dbUpdate() & dbLog() 
	  *
	  * @param string table name
	  * @param int value of id key identifying the row to be updated
	  * @param array associative array with data. Fields not matching columns of the table are silently ignored. 
	  * @param string the name of the id field if it is not 'id'
	  * @return int -1 on error, 0 if the query had no effect, 1 if an actual change occured (see mysql_affected_rows())
	  */
	public function _dbUpdate($tableName, $id, $data, $idName = "id") {
		if ( ! $this->isConnected ) {
			Mview::error("Mmodel not connected");
			return(-1);
		}
		$cols = $this->columns($tableName);
		if ( ! $cols )
			return(-1);

		$this->ammend($data, $cols);
		$origData = $this->getRow( $this->dbString( "select * from $tableName where $idName = '%s'", $id) );
		$pairs = array();
		foreach ( $data as $fname => $value ) {
			//if ( $fname == $idName || ! in_array($fname, $cols) || $this->equiv($origData[$fname], $value) )
			if ( ! in_array($fname, $cols) || $this->equiv($origData[$fname], $value) )
				continue;
            /*
            $dataType = $this->dataType($tableName, $fname);
            if ( $dataType == 'timestamp' )
                continue;
            if ( $dataType == 'date' && $value != null && ($value = Mdate::scan($value)) == null )
                    continue;
            if ( $dataType == 'datetime' && $value != null && ($value = Mdate::datetimeScan($value)) == null )
                    continue;
            */
			$str = $this->str($value);
			if ( $str === 'now()' )
				$pairs[] = "$fname = $str";
			elseif ( $str === null )
				$pairs[] = "$fname = null";
			else
				$pairs[] = "$fname = '$str'";
		}
		if ( ! $pairs ) {
			// nothing changed - do nothing else
			return(0);
		}
		$pairList = implode(", ", $pairs);
		$sql = $this->dbString( "update $tableName set $pairList where $idName = '%s'", $id); //echo $sql;
		$affected = $this->_sql($sql);
		return($affected);
	}
	/*--------------------*/
	/**
	  * update a row of data
	  *
	  * @param string table name
	  * @param int value of id key identifying the row to be updated
	  * @param array associative array with data. Fields not matching columns of the table are silently ignored. 
	  * @param string the name of the id field if it is not 'id'
	  * @return bool true if all is well, (including no-change), false on error
	  */
	public function dbUpdate($tableName, $id, $data, $idName = "id") {
		$affected = $this->_dbUpdate($tableName, $id, $data, $idName);
		if ( $affected > 0 )
			$this->dbLog($tableName, 'update', $id);
		return($affected);
	}
	/*----------------------------------------*/
	/**
	  * delete a row (without logging - see dbLog())
	  *
	  * @param string table name
	  * @param int value of id key identifying the row to be updated
	  * @param string the name of the id field if it is not 'id'
	  * @return int 1 on success, 0 if nothing was deleted, -1 if an error occured
	  */
	public function _dbDelete($tableName, $id, $idName = "id") {
		if ( ! $this->isConnected ) {
			Mview::error("Mmodel not connected");
			return(-1);
		}
		$sql = $this->dbString("delete from $tableName where $idName = '%s'", $id);
		$affected = $this->_sql($sql);
		return($affected);
	}
	/*----------------------------------------*/
	/**
	  * delete a row
	  *
	  * @param string table name
	  * @param int value of id key identifying the row to be updated
	  * @param string the name of the id field if it is not 'id'
	  * @return bool true if all is well, (including no-change), false on error
	  */
	public function dbDelete($tableName, $id, $idName = "id") {
		$affected = $this->_dbDelete($tableName, $id, $idName);
		if ( $affected > 0 )
			$this->dbLog($tableName, 'delete', $id);
		return($affected >= 0);
	}
	/*------------------------------------------------------------*/
	/**
	  * return sql representing the data in the table ( use dumpTable() to stream large tables )
	  *
	  * @param string the table name
	  * @param string single field to sort by or null to avoid sorting
	  * @return string sql statement(s) to (re-)insert data to the table
	  */
	public function tableDump($tableName, $orderBy = "id") {
		if ( $orderBy && $this->isColumn($tableName, $orderBy) )
			$ob = "order by $orderBy";
		else
			$ob = "";
		$ret = "drop table if exists $tableName;\n";
		$cr = $this->getRow("show create table $tableName");
		$crvalues = array_values($cr);
		$ret .= $crvalues[1].";\n";
		$rows = $this->getRows("select * from $tableName $ob");
		foreach ( $rows as $row )
			$ret .= $this->dbInsertSql($tableName, $row, true).";\n";
		return($ret);
	}
	/*------------------------------------------------------------*/
	/**
	  * stream an SQL dump of the table to the standard output
	  *
	  * @param string the table name
	  */
	public function dumpTable($tableName, $select = null) {
		$ai = $this->autoIncrement($tableName);
		if ( $ai )
			$ob = "order by $ai";
		else
			$ob = "";
		$datetime = date("Y-m-d G:i:s.u");
		if ( $select ) {
			$query = $select;
			echo "-- M::dumpTable - $tableName - $select\n";
		} else {
			$query = "select * from $tableName $ob";
			echo "-- M::dumpTable starting - $tableName - $datetime\n";
			echo "drop table if exists $tableName;\n";
			$cr = $this->getRow("show create table $tableName");
			$crvalues = array_values($cr);
			echo $crvalues[1].";\n";
		}
		$res = $this->query($query);
		if ( ! $res )
			return;
		while($row = @mysql_fetch_assoc($res))
			echo $this->dbInsertSql($tableName, $row, true).";\n";

		if ( ! $select ) {
			$datetime = date("Y-m-d G:i:s.u");
			echo "-- M::dumpTable done - $tableName - $datetime\n";
			echo "\n";
		}
	}
	/*------------------------------------------------------------*/
	/**
	  * the data type of a column in a table
	  *
	  * @param string the table name
	  * @param string the column name
	  * @return string the data type
	  */
	public function dataType($tableName, $fieldName) {
		static $cache = array();

		if ( isset($cache[$tableName][$fieldName]) )
			return($cache[$tableName][$fieldName]);
		$columnRows = $this->getRows("show columns from $tableName", 2*3600);
		foreach ( $columnRows as $col )
			$cache[$tableName][$col['Field']] = $col['Type'];

		if ( isset($cache[$tableName][$fieldName]) )
			return($cache[$tableName][$fieldName]);
		return(null);
	}
	/*------------------------------------------------------------*/
	/**
	 * provide data for jquery autocomplete
	 *
	 * the request url is of the form id=$tname-$fname&q=...
	 * e.g. 
	 *	$(".autocomplete", context).autocomplete("?className=Mmodel&action=autocomplete&id=" + $this.id);
	 */
	 public function autoComplete() {
		$id = explode('-', @$_REQUEST['id']);
		if ( ! $id ) {
			@syslog(LOG_ERR, __FILE__.":". __LINE__.": ".get_class().":".__FUNCTION__.": bad id args: ".$_SERVER['QUERY_STRING']);
			echo "autoComplete error\n";
			return;
		}
		$cnt = count($id);
		if ( $cnt == 2 )
			list($tname, $fname) = $id;
		elseif ( $cnt == 3 ) {
			list($dbname, $tname, $fname) = $id;
			if ( ! $this->selectDB($dbname) ) {
				@syslog(LOG_ERR, __FILE__.":". __LINE__.": ".get_class().":".__FUNCTION__.": bad id args: ".$_SERVER['QUERY_STRING']);
				echo "autoComplete: cannont use $dbname\n";
				return;
			}
		} else {
			@syslog(LOG_ERR, __FILE__.":". __LINE__.": ".get_class().":".__FUNCTION__.": bad id args: ".$_SERVER['QUERY_STRING']);
			echo "autoComplete error\n";
			return;
		}

	 	$q  = $_REQUEST['q'];
		$sql = "select distinct $fname from $tname where $fname like '$q%' order by $fname";
		$strings = $this->getStrings($sql);
		if ( ! $strings )
			return;
		$ret = implode("\n", $strings)."\n";
		echo $ret;
	 }
	/*------------------------------*/
	public function permit() {
		 // allows automatic Mautocomplete!!
		return(true);
	}
	/*------------------------------------------------------------*/
	/**
	 * update a single item in the database from parameters in $_REQUEST
	 * 
	 * saveFieldInfo() is used directly from the jQuery jeditable plugin
	 * as a complete server-side ajax updater/responder
	 */
	public function saveFieldInfo() {
		$elementId = $_REQUEST['id'];
		$value = $_REQUEST['value'];

			
		$nameId = explode("-", $elementId);
		$tname = $nameId[0];
		$fname = $nameId[1];
		$id = $nameId[2];
		$OldValue = $this->getString("select $fname from $tname where id = $id");

		// updating the database requires login credentials
		if ( ! Mlogin::get('MloginName') ) {
			echo "$OldValue";
			exit;
		}
		$dataType = $this->dataType($tname, $fname);
		if ( $dataType == 'date' ) {
			if ( ($value = Mdate::scan($value)) == null ) {
				echo "$OldValue";
				exit;
			}
		}
		if ( $dataType == 'time' ) {
			if ( ($value = Mtime::fmt($value)) == null ) {
				echo "$OldValue";
				exit;
			}
		}
		$sqlValue = $this->str($value);
		$sql = "update $tname set $fname = '$sqlValue' where id = $id";
		$affected = $this->sql($sql);
		$newValue = $this->getString("select $fname from $tname where id = $id");

		if ( $dataType == 'date' )
			$newValue = Mdate::fmt($newValue);
		elseif ( $dataType == 'time' )
			$newValue = Mtime::fmt($newValue);
		elseif ( $dataType == 'float' || $dataType == 'double' )
			$newValue = msuFloatFmt((float)$newValue);

		echo $newValue;
	}
	/*------------------------------------------------------------*/
	/**
	 * log database activity in a table called queryLog
	 *
	 * Normally only used as 'private',
	 * dbInsert(), dbUpdate(), dbDelete() and sql() attempt
	 * to log activities when successful changes occur,
	 * while _dbInsert(), _dbUpdate(), _dbDelete() and _sql() do not.
	 * 
	 * @param string table name
	 * @param operation performed
	 * @param int id of the affected row
	 * @param string the sql statement performing the operation
	 */
	public function dbLog($tname, $op, $tid, $querySql = null) {
		if ( ! $this->isTable('queryLog') )
			return;
		$row = array(
				'tname' => $tname,
				'op' => $op,
				'tid' => $tid,
				'querySql' => $querySql ? $querySql : $this->lastSql,
				'loginName' => Mlogin::get('MloginName'),
				'stamp' => 'now()'
			);
		$this->_dbInsert('queryLog', $row, false);
	}
	/*------------------------------------------------------------*/
	/**
	 * a database ready representation of now()
	 *
	 * @return string
	 */
	public function datetimeNow() {
		$today = Mdate::dash(Mdate::today());
		$now = Mtime::fmt(Mtime::now());
		$ret = "$today $now";
		return($ret);
	}
	/*------------------------------*/
	/**
	 * a database ready representation of now() in a given timezone
	 *
	 * @param string timezone  (see date_default_timezone_set())
	 * @return string
	 */
	public function datetimeNowInTZ($tz = null) {
		$todayInTZ = Mdate::dash(Mdate::todayInTZ($tz));
		$nowInTZ = Mtime::nowInTZ($tz, true);
		$ret = "$todayInTZ $nowInTZ";
		return($ret);
	}
	/*------------------------------------------------------------*/
	/**
	 * sql for a sample of rows from table
	 *
	 * @param string name of table
	 */
	public function sampleSql($table) {
		if ( ! $this->isTable($table) )
			return(null);
		$id = $this->autoIncrement($table);
		$fieldList = implode(',', $this->columns($table));

		$rowNum = $this->rowNum($table);
		if ( ! $id  ) {
			if ( $rowNum > 500 )
				$limit = "limit 500";
			else
				$limit = "";
			return("select $fieldList from $table $limit");
		}
		if ( $rowNum < 100 )
			$sql = "select $fieldList from $table order by $id";
		elseif ( $rowNum < 1000 )
			$sql = "select $fieldList from $table where $id % 10 = 0 order by $id";
		elseif ( $rowNum < 10000 )
			$sql = "select $fieldList from $table where $id % 100 = 0 order by $id";
		elseif ( $rowNum < 100000 )
			$sql = "select $fieldList from $table where $id % 1000 = 0 order by $id";
		else
			$sql = "select $fieldList from $table order by $id desc limit 100";
		return($sql);
	}
	/*------------------------------------------------------------*/
	public function name($tname, $fname, $id) {
		static $cache = array();
		if ( isset($cache[$tname][$fname][$id]) )
			return($cache[$tname][$fname][$id]);
		if ( ! @$cache[$tname] )
			$cache[$tname] = array();
		if ( ! @$cache[$tname][$fname] ) {
			$cache[$tname][$fname] = array();
			$rows = $this->getRows("select id,$fname from $tname", 30*60);
			foreach ( $rows as $row )
				$cache[$tname][$fname][$row['id']] = $row[$fname];
		}
		return(@$cache[$tname][$fname][$id]);
	}
	/*------------------------------*/
	public function id($tname, $fname, $name, $make = false) {
		static $cache = array();
		if ( isset($cache[$tname][$fname][$name]) )
			return($cache[$tname][$fname][$name]);
		if ( ! @$cache[$tname] )
			$cache[$tname] = array();
		if ( ! @$cache[$tname][$fname] ) {
			$cache[$tname][$fname] = array();
			$rows = $this->getRows("select id,$fname from $tname", 30*60);
			foreach ( $rows as $row )
				$cache[$tname][$fname][$row[$fname]] = $row['id'];
		}
		if ( @$cache[$tname][$fname][$name] )
			return($cache[$tname][$fname][$name]);
		if ( ! $make )
			return(null);
		$id = $this->_dbInsert($tname, array(
			$fname => $name,
		));
		if ( ! $id )
			return(null);
		$cache[$tname][$fname][$name] = $id;
		return($cache[$tname][$fname][$name]);
	}
	/*------------------------------------------------------------*/
	public function lastError() {
		return($this->lastError);
	}
	/*------------------------------------------------------------*/
	/*------------------------------------------------------------*/
	/*------------------------------------------------------------*/
	private function ammend(&$data, $cols, $insert = false) {
		$loginName = Mlogin::get('MloginName');
		if ( $insert && in_array('createdOn', $cols) && ! isset($data['createdOn']) )
			$data['createdOn'] = date("Y-m-d G:i:s");
		if ( $loginName && $insert && in_array('createdBy', $cols) && ! isset($data['createdBy']) )
			$data['createdBy'] = $loginName;
		if ( in_array('lastChange', $cols) && ! isset($data['lastChange']) )
			$data['lastChange'] = date("Y-m-d G:i:s");
		if ( $loginName && in_array('lastChangeBy', $cols) && ! isset($data['lastChangeBy']) )
			$data['lastChangeBy'] = $loginName;
	}
	/*------------------------------*/
	private function dbInsertSql($tableName, $data, $withId = false) {
		$cols = $this->columns($tableName);
		if ( ! $cols )
			return(null);
		$row = array();
		//$idName = $this->autoIncrement($tableName);
		foreach ( $data as $fname => $value ) {
			if ( ! in_array($fname, $cols) )
				continue;
            /*
            if ( $fname == $idName )
                continue;
            $dataType = $this->dataType($tableName, $fname);
            if ( $dataType == 'timestamp' )
                continue;
            if ( $dataType == 'date' && ($value = Mdate::scan($value)) == null )
                    continue;
            if ( $dataType == 'datetime' && ($value = Mdate::datetimeScan($value)) == null )
                    continue;
            */
            $str = $value;

            if ( $str === 'now()' )
                $row[$fname] = 'now()';
            elseif ( $str === null )
                $row[$fname] = 'null';
            else
                $row[$fname] = "'".$this->str($str)."'";

        }
        /*
        $insertData = array();
        foreach ( $row as $fname => $value )
                $insertData[$fname] = $value;
        $this->ammend($insertData, $cols, true);
        */
        $fieldList = implode(',', array_keys($row));
        $valuesList = implode(",", $row);

        /*
        $values = array();
        foreach ( $insertData as $value )
            if ( $value === 'now()' )
                $values[] = 'now()';
            else
                $values[] = "'".$this->str($value)."'";
        $valuesList = implode(",", $values);
        */

        $sql = "insert into $tableName ( $fieldList ) values ( $valuesList )";
        return($sql);
    }
    /*------------------------------------------------------------*/
	/*
	 * do fields have equivalent values:
	 * nulls and empty strings, dates in int or dashed format are equivalent
	 * updating of equivalent values is skipped
	 */
	private function equiv($a, $b) {
		if ( $a == $b )
			return(true);
		$alen = strlen($a);
		$blen = strlen($b);
		// dates
		if (
			( $alen == 10 || $alen == 8 )
			&& ( $blen == 10 || $blen == 8 )
			&& str_replace('-', '', $a) == str_replace('-', '', $b)
			)
			return(true);
		// text
		if (
			str_replace("\r\n", "\n", $a) == str_replace("\r\n", "\n", $b)
			)
			return(true);
		return(false);
	}
	/*------------------------------------------------------------*/
}
/*------------------------------------------------------------*/
