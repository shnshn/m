<?php
/**
 * Created by SMSsenger.
 * Date: 9/22/14
 * Time: 5:24 PM
 */

/**
 * Mmodel and Mview are accessible from Mcontroller and any class
 * that extends Mcontroller
 */
require_once("Mmodel.class.php");
require_once("Mview.class.php");

class Mrouter {
    public $controller;
    public $action;
    private $registry;

    public function __construct() {
        $this->registry = Mregistry::getInstance();
        $this->Mmodel = Mmodel::getInstance();
        $this->Mview = Mview::getInstance();

        date_default_timezone_set("Asia/Jerusalem");
    }

    private function requestArgs($args) {
        $requestArgs = array();
        if ( is_string($args) ) {
            $vars = explode('&', $args);
            foreach ( $vars as $var ) {
                $nv = explode('=', $var);
                if ( count($nv) != 2 ) {
                    $this->Mview->error("$var ???");
                    continue;
                }
                list($n, $v) = $nv;
                $requestArgs[$n] = $v;
            }
        } else if ( $args ) {
            foreach ( $args as $key => $arg )
                $requestArgs[$key] = $arg;
        }

        return $requestArgs;
    }

    /*------------------------------------------------------------*/
    private function obj($className) {
        $controllerClassName = $className."Controller";
        if ( ($className = $this->className($className)) == null )
            return($this);
        if ( class_exists($controllerClassName) ) {
            $obj = new $controllerClassName;
            return($obj);
        }



        //$files = Mutils::listDir(".", "php");
        // git problems - Fri Dec 14 15:06:35 IST 2012
        foreach ( glob("*.php") as $file ) {
            $fileParts = explode(".", $file);
            $baseName = reset($fileParts);
            if(strtolower($className) != strtolower($baseName) )
                continue;
            require_once($file);

            $baseNameController = $baseName."Controller";
            if ( class_exists($baseNameController) ) {
                $obj = new $baseNameController;
                return($obj);
            }
            $this->Mview->error("class $baseName | $baseNameController no found in $file");
        }
        /*	$this->Mview->error("cannot find class for '$className' in '".implode(",", $files));	*/
        $this->Mview->error("cannot find class for '$className'");
        return(null);
    }

    public function loader($className = null, $action = null, $args = null) {

        $requestArgs = $this->requestArgs($args);

        $controller = $this->obj($className);
        if ( ! $controller )
            return(null);

        $action = $this->action($action);
        if ( $action == null )
            $action = "index";

        if ( ! is_callable(array($controller, $action)) ) {
            $className = get_class($controller);
            $this->Mview->error("Mcontroller: Method '$action' not callable in class '$className'");
            return(null);
        }
        $className = get_class($controller);

        $this->controller = strtolower($className);
        $this->action = strtolower($action);
        //$obj->controller = strtolower($className);
        //$obj->action = strtolower($action);

        $savedRequestArgs = $this->setRequestArgs($requestArgs);
        if ( ! $controller->permit($className, $action) ) {
            return(null);
        }

        if ( method_exists($controller, "before") ) // e.g. Mmodel auto-autocomplete does not extend Mcontroller
            $controller->before();
        $controller->$action();
        if ( method_exists($controller, "after") ) // e.g. Mmodel auto-autocomplete does not extend Mcontroller
            $controller->after();
        $this->revertRequestArgs($requestArgs, $savedRequestArgs);
    }

    /*------------------------------*/
    private function setRequestArgs($requestArgs) {
        $savedRequestArgs = array();
        foreach ( $requestArgs as $key => $arg ) {
            if ( array_key_exists($key, $_REQUEST) )
                $savedRequestArgs[$key] = $_REQUEST[$key];
            $_REQUEST[$key] = $arg;
        }
        return($savedRequestArgs);
    }
    /*------------------------------*/
    private function revertRequestArgs($requestArgs, $savedRequestArgs) {
        foreach ( $requestArgs as $key =>  $arg )
            unset($_REQUEST[$key]);
        foreach ( $savedRequestArgs as $key =>  $arg )
            $_REQUEST[$key] = $arg;
    }

    /*------------------------------------------------------------*/
    public function pathParts() {
        if ( isset($_REQUEST['PATH_INFO']) )
            $path = $_REQUEST['PATH_INFO'];
        elseif ( isset($_SERVER['PATH_INFO']) )
            $path = $_SERVER['PATH_INFO'];
        else
            return(null);

        // ignore leading, trailing and duplicate slashes
        $pathParts = array();
        $parts = explode("/", $path);
        foreach ( $parts as $part )
            if ( $part != "" )
                $pathParts[] = $part;

        return($pathParts);
    }

    /*------------------------------------------------------------*/
    private function className($className = null) {
        if ( $className )
            return($className);
        if ( isset($_POST['className']) && $_POST['className'] != '' )
            return($_POST['className']);
        if ( isset($_GET['className']) && $_GET['className'] != '' )
            return($_GET['className']);
        $pathParts = $this->pathParts();
        return(isset($pathParts[0]) ? $pathParts[0] : null);
    }

    /*------------------------------------------------------------*/
    private function action($action = null) {
        if ( $action )
            return($action);
        if ( isset($_POST['action']) && $_POST['action'] != '' )
            return($_POST['action']);
        if ( isset($_GET['action']) && $_GET['action'] != '' )
            return($_GET['action']);
        $pathParts = $this->pathParts();
        if ( isset($pathParts[1]) )
            return($pathParts[1]);
        return(null);
    }
} 