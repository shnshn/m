<?php
/*------------------------------------------------------------*/
/**
  * @package M
  * @author Ohad Aloni
  */
/*------------------------------------------------------------*/
/**
  * The is no bootstrap and dispatcher separate from Mcontroller
  * This is quite diffrent than most PHP MVC frameworks
  * 
  *  Using M mostly means:
  *     setting database access as described in Mconfig.php<br />
  *     'requiring' this file, extending Mcontroller and calling control() from the extended class.<br />
  *     see Mdemo/index.php for an example<br />
  */
/*------------------------------------------------------------*/
require_once("Mmemcache.class.php");
require_once("Perf.class.php");
require_once("Mmodel.class.php");
require_once("Mview.class.php");
require_once("Mcontroller.class.php");
require_once("Mrouter.class.php");
require_once("Mregistry.class.php");
require_once("Msession.class.php");
require_once("Mlogin.class.php");
require_once("Mdate.class.php");
require_once("Mtime.class.php");
require_once("Mutils.class.php");
require_once("Mtable.class.php");
require_once("Mquery.class.php");
require_once("msu.php");
/*------------------------------------------------------------*/
/*------------------------------------------------------------*/