<?php
/*------------------------------------------------------------*/
class Mmemcache {
	/*------------------------------------------------------------*/
	private $memcache = null;
	private $version = 20;
	private static $debugLevel;
	/*------------------------------------------------------------*/
	public function __construct($debugLevel = 0) {
		self::$debugLevel = $debugLevel;
		$me = get_class()."::".__FUNCTION__."()";
		if ( class_exists("Memcache") ) {
			$memcache = new Memcache;
			try {
				if ( @$memcache->connect("localhost") ) {
					$this->memcache = $memcache;
					$this->memcache->setCompressThreshold(10000, 0.3);
				} else {
					static $visited = false;
					if ( ! $visited ) {
						$visited = true;
						$this->debug(__FILE__, __LINE__, $me, "Memcache down");
					}
				}
			} catch ( Exception $e ) {
				static $visited = false;
				if ( ! $visited ) {
					$visited = true;
					$error = $e->getMessage();
					$this->debug(__FILE__, __LINE__, $me, $error);
				}
			}
		}
	}
	/*------------------------------------------------------------*/
	public function get($key) {
		$me = get_class()."::".__FUNCTION__."()";
		if ( ! $this->memcache )
			return(false);
		$vkey = $this->versionKey($key);
		$get = @$this->memcache->get($vkey);
		if ( $get === false ) {
			$this->debug(__FILE__, __LINE__, $me, "get miss: '$key'", 3);
			return(false);
		}
		$this->debug(__FILE__, __LINE__, $me, "get hit: $key", 10);
		$get = $this->unpack($get);
		if ( $get === false )
			$this->debug(__FILE__, __LINE__, $me, "get hit: MISS?? '$key'");
		return($get);
	}
	/*------------------------------*/
	public function set($key, $value, $ttl = null) {
		$me = get_class()."::".__FUNCTION__."()";
		if ( ! $this->memcache )
			return(false);
		if ( $ttl === null )
			$ttl = 15*60;
		$versionKey = $this->versionKey($key);
		$value = $this->pack($value);
		$setOK = $this->memcache->set($versionKey, $value, 0, $ttl);
		if ( $setOK )
			$this->debug(__FILE__, __LINE__, $me, "'$key'", 4);
		else
			$this->debug(__FILE__, __LINE__, $me, "'$key' NOT SET");
			
		return($setOK);
	}
	/*------------------------------------------------------------*/
	/*------------------------------------------------------------*/
	public function msgQadd($qname, $msg) {
		$me = get_class()."::".__FUNCTION__."()";
		if ( ! $this->memcache )
			return(false);
		$firstIdKey = $this->msgQfirstIdKey($qname);
		$lastIdKey = $this->msgQlastIdKey($qname);
		$lastId = $this->memcache->get($lastIdKey);
		if ( ! $lastId ) {
			$this->memcache->set($firstIdKey, 1, false, 0);
			$this->memcache->set($lastIdKey, 0, false, 0);
		}
		$lastId = $this->memcache->increment($lastIdKey);
		$bundle = array(
			'id' => $lastId,
			'queued' => time(),
			'msg' => $msg,
		);
		$idKey = $this->msgQidKey($qname, $lastId);
		$this->memcache->set($idKey, $bundle, false, 0);
		return(true);
	}
	/*------------------------------*/
	public function msgQlength($qname) {
		return(count($this->msgQ($qname)));
	}
	/*------------------------------*/
	// read only the entire queue
	// with bundle info
	public function msgQ($qname) {
		$me = get_class()."::".__FUNCTION__."()";
		if ( ! $this->memcache )
			return(null);
		$firstIdKey = $this->msgQfirstIdKey($qname);
		$lastIdKey = $this->msgQlastIdKey($qname);
		$firstId = $this->memcache->get($firstIdKey);
		if ( ! $firstId )
			return(null);
		$lastId = $this->memcache->get($lastIdKey);
		$msgQ = array();
		for ( $id = $firstId ; $id <= $lastId ; $id++ )
			$msgQ[] = $this->memcache->get($this->msgQIdKey($qname, $id));
		return($msgQ);
	}
	/*------------------------------*/
	// null on empty, false on error
	public function msgQnext($qname) {
		$me = get_class()."::".__FUNCTION__."()";
		if ( ! $this->memcache )
			return(null);
		$firstIdKey = $this->msgQfirstIdKey($qname);
		$firstId = $this->memcache->get($firstIdKey);
		if ( ! $firstId )
			return(null);
		$lastIdKey = $this->msgQLastIdKey($qname);
		$lastId = $this->memcache->get($lastIdKey);
		// the q is empty
		if ( $firstId > $lastId )
			return(null);
		// at least this item is in the queue
		$idKey = $this->msgQidKey($qname, $firstId);
		$bundle = $this->memcache->get($idKey);
		$this->memcache->delete($idKey);
		$this->memcache->increment($firstIdKey);
		// a bad id is an error in this code (Mmemcache)!!!
		if ( $bundle['id'] != $firstId ) {
			return(false);
		}
		$maxWait = 5*60;
		// clear conjested queue
		if ( (time() - $bundle['queued']) > $maxWait )
			return(false);
		return($bundle['msg']);
	}
	/*------------------------------------------------------------*/
	/*------------------------------------------------------------*/
	/*------------------------------------------------------------*/
	private function versionKey($key) {
		$version = $this->version;
		$versionKey = "$version:$key";
		$versionKey = sha1($versionKey);
		return($versionKey);
	}
	/*------------------------------*/
	// 2012-11-15/23:48:06
	// memcache fails to store short strings ?
	// but not small arrays
	// pack all values into arrays
	//	Sat Nov 24 14:27:33 IST 2012
	//  this did not work for a null value
	// its important to store the empty results
	// of sql queries, or else the db will be consulted again.
	// so adding to pack array to make it have some content
	/*--------------------*/
	private function pack($value) {
		return(array(
			'dummyContent' => 'The big brown fox jumps over a lazy dog',
			'value' => $value,
		));
	}
	/*--------------------*/
	private function unpack($value) {
		// compat
		if ( ! isset($value['value']) )
			return($value[0]);
		return($value['value']);
	}
	/*------------------------------*/
	private function msgQidKey($qname, $id) {
		$msgQidKey = $this->versionKey("$qname:$id");
		return($msgQidKey);
	}
	/*------------------------------*/
	private function msgQfirstIdKey($qname) {
		$msgQfirstIdKey = $this->versionKey("$qname:msgQfirstIdKey");
		return($msgQfirstIdKey);
	}
	/*------------------------------*/
	private function msgQlastIdKey($qname) {
		$msgQLastIdKey = $this->versionKey("$qname:msgQLastIdKey");
		return($msgQLastIdKey);
	}
	/*------------------------------------------------------------*/
	private function debug($file, $lineNo, $tag, $msg = null, $debugLevelAtLeast = 1) {
		$debugLevel = Mutils::getenv("debugLevel");
		if ( $debugLevel < $debugLevelAtLeast )
			return;
		date_default_timezone_set("Asia/Jerusalem");
		$datetime = date("Y-m-d G:i:s");
		$fileName = basename($file);

		$text = "$datetime: $fileName:$lineNo:$tag";
		if ( $msg )
			$text .= ": $msg";
		if ( @$_SERVER['SERVER_ADDR'] )
			$text .= "<br />";
		echo "$text\n";
	}
	/*------------------------------------------------------------*/
	/*------------------------------------------------------------*/
	/*------------------------------------------------------------*/
}
/*------------------------------------------------------------*/
