<?php
/*------------------------------------------------------------*/
class Mlogin {
	/*------------------------------------------------------------*/
	public static function login($userId, $verification) {
		$expires = 2*24*60*60;
		Mview::setCookie("SID", $userId, $expires);
		Mview::setCookie("SVF", $verification, $expires);
	}
	/*------------------------------------------------------------*/
	public static function logout() {
		Mview::setCookie("SID", null, -1);
		Mview::setCookie("SVF", null, -1);
	}
	/*------------------------------------------------------------*/
	public static function getLoginValues() {
		$ret = array();
		$ret['userId'] = intval(self::get("SID"));
		$ret['md5'] = self::get("SVF");
		
		return($ret);
	}
	/*------------------------------------------------------------*/
	public static function get($var) {
		return(@$_COOKIE[$var]);
	}
	/*------------------------------------------------------------*/
	public static function is() {
		$is = @$_COOKIE['SID'] != null;
		if ( $is )
			self::stay();
		return($is);
	}
	/*------------------------------*/
	private static function stay() {
		$userId = self::get("SID");
		if ( ! $userId )
			return;
		$verification = self::get("SVF");
		self::login($userId, $verification);
	}
	/*------------------------------------------------------------*/
}
/*------------------------------------------------------------*/
