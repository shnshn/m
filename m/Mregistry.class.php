<?php
/**
 * Created by SMSsenger.
 * Date: 9/28/14
 * Time: 10:30 AM
 */

class Mregistry {
    private static $_instance; //The single instance

    private $vars = array();
    private $_errorStr = '';

    public function __set($index, $value)
    {
        $this->vars[$index] = $value;
    }

    public function __get($index)
    {
        if( !isset($this->vars[$index]) ) return null;
        return $this->vars[$index];
    }

    public function errorStr($error=null) {
        if($error == null) return $this->_errorStr;

        $this->_errorStr = $error;
    }

    /*------------------------------------------------------------*/
    /*
    Get an instance of the Mview
    @return Instance
    */
    public static function getInstance() {
        if(!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
        }
        return self::$_instance;
    }
} 